# AESIR-PRHA #



### What is AESIR-PRHA? ###

**AESIR-PRHA** (**A**utomated **E**nvironmental **S**urveillance & **I**nteractive **R**eports – **P**uerto **R**ico **H**urricane **A**ftermath) 
is an AI-enabled map of tagged features in Puerto Rico after Hurricane Maria.